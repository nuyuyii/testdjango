Overview
========
- บันทึกเงินออมในแต่ละวัน


Features
========

- ประมวลผลที่เงินที่ออมทั้งหมด
- คิดค่าเงินออมมากสุด น้อยสุดต่อวันที่เคยออม
- เรียงค่ามากสุดไปน้อยสุด 
- เรียงค่าน้อยสุดไปมากสุด
- ลบหน้าข้อมูลการออม



Use cases
=========

คำนวณเงินออม
-----------

1. จำนวนเงินที่ต้องการออมต่อเดือนหรือต่อวัน ตรงนี้เป็นตัวเลือก [รับค่าเป็นตัวเลข]
2. วันที่ออม [รับค่าเป็นตัวเลข]
3. บันทึกการออม
4. ยอดรวมเงินออมที่ได้ทั้งหมด
5. แสดงค่า max min เงินที่เคยออม

เรียงค่า
-----------

1. เรียงค่าเงินออมจากมากไปน้อย
2. เรียงค่าเงินออมจากน้อยไปมาก

ล้างข้อมูล
-----------

1. ล้างข้อมูลหน้าการคำนวณทั้งหมด