# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0002_auto_20150402_0611'),
    ]

    operations = [
        migrations.AddField(
            model_name='money',
            name='avg',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]
