from django.template.loader import render_to_string
from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from lists.views import home_page
from lists.models import money
import unittest
import pep8


class TestCodeFormat(unittest.TestCase):

    def test_pep8_conformance(self):
        """Test that we conform to PEP8."""
        pep8style = pep8.StyleGuide(quiet=True)
        result = pep8style.check_files(['urls.py', 'views.py'])
        # pep8 urls.py
        fchecker = pep8.Checker('TestDjango/urls.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings)" % file_errors)
        fchecker = pep8.Checker('lists/models.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) lists/models.py" % file_errors)
        fchecker = pep8.Checker('lists/views.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) lists/views.py" % file_errors)
        fchecker = pep8.Checker('functional_tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) functional_tests.py"
              % file_errors)
        fchecker = pep8.Checker('lists/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings) lists/tests.py" % file_errors)


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        # self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['date'] = '17/3/2558'
        request.POST['cost'] = '101'
        request.POST['button_send'] = 'send'

        response = home_page(request)

        new_item = money.objects.first()
        self.assertEqual(new_item.date, '17/3/2558')
        self.assertEqual(new_item.cost, 101.0)

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(money.objects.count(), 0)

    def test_home_page_displays_all_list_items(self):
        money.objects.create(cost='100')
        money.objects.create(cost='200')

        request = HttpRequest()
        response = home_page(request)
        self.assertIn('100', response.content.decode())  # first value
        self.assertIn('200', response.content.decode())  # second value
        self.assertIn('300', response.content.decode())  # total
        self.assertIn('150', response.content.decode())  # average


class ItemModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_item = money()
        first_item.cost = '100'
        first_item.save()

        second_item = money()
        second_item.cost = '200'
        second_item.save()

        saved_items = money.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.cost, 100.0)
        self.assertEqual(second_saved_item.cost, 200.0)
