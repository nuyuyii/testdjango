from django.shortcuts import redirect, render
from lists.models import money
import operator


def home_page(request):
    if(request.method == 'POST' and
       request.POST.get('delete', '') == 'delete'):
        id_data = request.POST['id_delete']
        money.objects.get(pk=id_data).delete()
        return redirect('/')
    if(request.method == 'POST'and
       request.POST.get('button_send', '') == 'send'):
        date_text = request.POST['date']
        cost_text = float(request.POST['cost'])
        money.objects.create(
                date=date_text,
                cost=cost_text
               )
        return redirect('/')
    moneys = money.objects.all()
    cost = 0
    avg = 0
    max_money = 0
    min_money = 0
    data = []
    max_min = []
    min_max = []
    for m in moneys:
        cost = cost+m.cost
        data.append(m.cost)

    if money.objects.count() != 0:
        avg = cost/money.objects.count()
        max_money = max(data)
        min_money = min(data)
        for i in range(len(data)):
            value = data[i]
            min_max.append(value)
            max_min.append(value)
        min_max.sort()
        max_min.sort(reverse=True)

    return render(request, 'home.html',
                  {'moneys': moneys, 'cost_money': cost, 'avg_money': avg,
                   'max_money': max_money, 'min_money':  min_money,
                   'min_max': min_max, 'max_min': max_min})
