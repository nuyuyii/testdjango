from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import unittest
import datetime
import time


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_column_in_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_total_in_table(self, row_text):
        table = self.browser.find_element_by_id('id_total_table')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_max_min_in_table(self, row_text):
        table = self.browser.find_element_by_id('id_max_min_table')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def test_send_data(self):
        self.browser.get('http://localhost:8000')  # time.sleep(3)
        self.assertIn('money', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('save', header_text)
        # test1 input box
        inputbox_date = self.browser.find_element_by_id('date_new')
        self.assertEqual(
                inputbox_date.get_attribute('placeholder'),
                'Enter date'
        )
        inputbox_money = self.browser.find_element_by_id('cost_new')
        self.assertEqual(
                inputbox_money.get_attribute('placeholder'),
                'Enter money'
        )
        inputbox_date.send_keys('01/04/2558')
        inputbox_money.send_keys('200')
        button = self.browser.find_element_by_id('button')
        button.click()
        time.sleep(3)
        self.check_for_column_in_table('01/04/2558')
        self.check_for_column_in_table('200.0')
        # test 2
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_money = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('02/04/2558')
        inputbox_money.send_keys('100')
        button = self.browser.find_element_by_id('button')
        button.click()
        time.sleep(3)
        self.check_for_column_in_table('02/04/2558')
        self.check_for_column_in_table('100.0')
        # test 3
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_money = self.browser.find_element_by_id('cost_new')
        # Day = datetime.date.today()
        inputbox_date.send_keys('03/04/2558')  # Day.strftime('%d/%m/%Y'))
        inputbox_money.send_keys('75')
        button = self.browser.find_element_by_id('button')
        button.click()
        time.sleep(3)
        self.check_for_column_in_table('03/04/2558')
        self.check_for_column_in_table('75.0')
        self.check_for_total_in_table('ผลรวม')
        self.check_for_total_in_table('375.0')
        self.check_for_total_in_table('ค่าเฉลี่ย')
        self.check_for_total_in_table('125.0')
        self.check_for_max_min_in_table('75.0')
        self.check_for_max_min_in_table('200.0')
        self.fail('Finish the test!')


if __name__ == '__main__':
    unittest.main(warnings='ignore')
